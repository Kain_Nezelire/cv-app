// src/pages/Inner.jsx

import React, { useState, useEffect } from 'react';
import Address from '../components/Address/Address';
import Box from '../components/Box/Box';
import Navigation from '../components/Navigation/Navigation';
import Expertise from '../components/Expertise/Expertise';
import Portfolio from '../components/Portfolio/Portfolio';
import Feedback from '../components/Feedback/Feedback';
import Skills from '../features/skills/skills';
import Education from '../components/Educations/education';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { animateScroll as scroll } from 'react-scroll';

const ContentWrapper = styled.div`
  margin-left: ${(props) => (props.isOpen ? '250px' : '60px')};
  padding: 20px;
  transition: margin-left 0.3s;
`;

const Section = styled.section`
  padding: 20px 0;
`;

const FullPage = styled.div`
  opacity: 1;
  z-index: 500;
`;

const BurgerButton = styled.button`
  background: rgb(31, 42, 40);
  border: none;
  color: white;
  font-size: 1.5em;
  cursor: pointer;
  position: fixed;
  top: 20px;
  left: ${(props) => (props.isOpen ? '250px' : '60px')};
  transition: left 0.3s;
  z-index: 1100;

  @media (max-width: 768px) {
    display: none;
  }
`;

const ScrollButton = styled.button`
  background: rgb(31, 42, 40);
  border: none;
  color: white;
  font-size: 1.5em;
  cursor: pointer;
  position: fixed;
  bottom: 20px;
  left: ${(props) => (props.isOpen ? '220px' : '1280px')};
  transition: left 0.3s;
  z-index: 1100;

  @media (max-width: 768px) {
    display: none;
  }
`;

const dataForExperience = [
  {
    date: '2013',
    info: { company: 'Google', job: 'Front-end developer / php programmer' },
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit...',
  },
  {
    date: '2012',
    info: { company: 'Twitter', job: 'Web developer' },
    description: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget...',
  },
];

const feedbackData = [
  {
    feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit...',
    reporter: {
      photoUrl: './user.jpg',
      name: 'John Doe',
      citeUrl: 'https://www.citeexample.com',
    },
  },
  {
    feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit...',
    reporter: {
      photoUrl: './user.jpg',
      name: 'John Doe',
      citeUrl: 'https://www.citeexample.com',
    },
  },
];

const Inner = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 768);

  const toggleSidebar = () => {
    if (!isMobile) {
      setIsOpen(!isOpen);
    }
  };

  const scrollToTop = () => {
    scroll.scrollToTop();
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (
    <FullPage>
      <BurgerButton onClick={toggleSidebar} isOpen={isOpen}>
        <FontAwesomeIcon icon={faBars} />
      </BurgerButton>
      <Navigation isOpen={isOpen} />
      <ContentWrapper isOpen={isOpen}>
        <Section id="about">
          <Box title="About me" content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit..." />
        </Section>
        <Section id="education">
          <Box title="Education" content={<Education />} />
        </Section>
        <Section id="experience">
          <Box title="Experience" content={<Expertise data={dataForExperience} />} />
        </Section>
        <Section id="portfolio">
          <Box title="Portfolio" content={<Portfolio />} />
        </Section>
        <Section id="skills">
          <Box title="Skills" content={<Skills />} />
        </Section>
        <Section id="contacts">
          <Box title="Contacts" content={<Address />} />
        </Section>
        <Section id="feedback">
          <Box title="Feedback" content={<Feedback data={feedbackData} />} />
        </Section>
      </ContentWrapper>
      <ScrollButton onClick={scrollToTop} isOpen={isOpen}>
        <FontAwesomeIcon icon={faArrowUp} />
      </ScrollButton>
    </FullPage>
  );
};

export default Inner;
