// src/pages/Home.jsx
import React from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../components/Button/Button';
import PhotoBox from '../components/PhotoBox/PhotoBox';
import { Page, GlobalStyle, Content } from './HomeStyle';


const Home = () => {
  const navigate = useNavigate();

  const handleButtonClick = () => {
    navigate('/inner');
  };

  return (
    <>
      <GlobalStyle />
      <Page>
        <Content>
          <PhotoBox />
            <h1>Kairzhan Yergozha</h1>
            <h2>Programmer. Creative. Innovator</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas velit amet, quidem iure omnis reprehenderit fugiat, </p>
            <Button buttonText={'Read More'} onClick={handleButtonClick} />
        </Content>
      </Page>
    </>
  );
};

export default Home;
