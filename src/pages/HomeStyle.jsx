// src/pages/HomeStyle.js
import styled, { createGlobalStyle } from "styled-components";
import forest from "../assets/images/Forest.jpg";

// Глобальные стили
export const GlobalStyle = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
  }
`;



export const Page = styled.div`
  position: relative;
  background-image: url(${forest});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-attachment: fixed;
  background-color: #f0f0f0;
  height: 100vh;
  width: 100%; 
  margin: 0; 
  padding: 0; 

  &::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.5); 
  }
`;

export const Content = styled.div`
  position: relative;
  z-index: 2;
  color: white; 
  height:100%;
  width:100%; 
  display:flex;
  flex-direction:column;
  justify-content:center;
  align-items:center;
  text-align:center;
  p{
    max-width:50%;
  }
`;
