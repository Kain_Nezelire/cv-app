import { configureStore } from '@reduxjs/toolkit';
import educationReducer from '../features/education/educationSlice';
import timelineReducer from '../features/timeline/TimelineSlice';
import skillsReducer from '../features/skills/skillsSlice';

const store = configureStore({
  reducer: {
    education: educationReducer,
    timeline: timelineReducer,
    skills: skillsReducer,
  },
});

export default store;
