import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Inner from './pages/Inner';

const App = () => (
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/inner" element={<Inner />} />
  </Routes>
);

export default App;
