import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

// Начальные данные навыков
const initialSkills = [
  { id: 1, name: 'React', level: 90 },
  { id: 2, name: 'CSS', level: 80 },
  { id: 3, name: 'HTML', level: 70 },
  { id: 4, name: 'JavaScript', level: 85 },
];

// Функция для получения навыков из localStorage или установки начальных данных
const getInitialSkills = () => {
  const savedSkills = localStorage.getItem('skills');
  if (savedSkills) {
    return JSON.parse(savedSkills);
  } else {
    localStorage.setItem('skills', JSON.stringify(initialSkills));
    return initialSkills;
  }
};

export const fetchSkills = createAsyncThunk('skills/fetchSkills', async () => {
  const response = await axios.get('/api/skills');
  const fetchedSkills = response.data;

  // Сохранение полученных навыков в localStorage
  localStorage.setItem('skills', JSON.stringify(fetchedSkills));
  return fetchedSkills;
});

const skillsSlice = createSlice({
  name: 'skills',
  initialState: {
    items: getInitialSkills(),
    status: 'idle',
    error: null,
  },
  reducers: {
    addSkill: (state, action) => {
      const newSkill = {
        id: state.items.length + 1,
        name: action.payload.name,
        level: action.payload.level,
      };
      state.items.push(newSkill);

      // Сохранение обновленного списка навыков в localStorage
      localStorage.setItem('skills', JSON.stringify(state.items));

      // Отправка нового навыка на сервер
      axios.post('/api/skills', newSkill)
        .catch((error) => {
          console.error('Ошибка при отправке данных на сервер:', error);
        });
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchSkills.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.items = action.payload;
        localStorage.setItem('skills', JSON.stringify(action.payload));
      })
      .addCase(fetchSkills.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  },
});

export const { addSkill } = skillsSlice.actions;

export default skillsSlice.reducer;
