import styled from 'styled-components';

export const SkillsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

export const SkillItem = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0;
`;

export const SkillName = styled.div`
  flex: 1;
  font-weight: bold;
`;

export const SkillLevel = styled.div`
  flex: 1;
  text-align: right;
`;

export const EditButton = styled.button`
  align-self: flex-end;
  background: none;
  border: none;
  color: #36cfc9;
  cursor: pointer;
  font-size: 16px;
  padding: 10px;
  margin-bottom: 10px;

  &:hover {
    color: #009688;
  }
`;

export const SkillForm = styled.form`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;

  label {
    display: flex;
    flex-direction: column;
    margin-bottom: 10px;
  }

  input {
    padding: 10px;
    margin-top: 5px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }

  button {
    padding: 10px;
    background-color: #36cfc9;
    color: white;
    border: none;
    border-radius: 4px;
    cursor: pointer;

    &:hover {
      background-color: #009688;
    }
  }
`;
