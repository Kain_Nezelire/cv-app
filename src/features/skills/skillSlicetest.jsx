import reducer, { addSkill, fetchSkills } from './skillsSlice';
import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { expect } from '@jest/globals';

jest.mock('axios');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// Начальные данные навыков для тестов
const initialSkills = [
  { id: 1, name: 'React', level: 90 },
  { id: 2, name: 'CSS', level: 80 },
  { id: 3, name: 'HTML', level: 70 },
  { id: 4, name: 'JavaScript', level: 85 },
];

describe('skillsSlice', () => {
  describe('reducers', () => {
    const initialState = {
      items: initialSkills,
      status: 'idle',
      error: null,
    };

    it('должен вернуть начальное состояние', () => {
      expect(reducer(undefined, { type: undefined })).toEqual(initialState);
    });

    it('должен добавить новый навык', () => {
      const previousState = initialState;
      const newSkill = { name: 'Node.js', level: 75 };
      expect(reducer(previousState, addSkill(newSkill))).toEqual({
        ...previousState,
        items: [...previousState.items, { id: previousState.items.length + 1, ...newSkill }],
      });
    });
  });

  describe('extraReducers', () => {
    it('должен установить статус загрузки при fetchSkills.pending', () => {
      const previousState = {
        items: [],
        status: 'idle',
        error: null,
      };
      expect(reducer(previousState, fetchSkills.pending())).toEqual({
        ...previousState,
        status: 'loading',
      });
    });

    it('должен установить полученные данные и статус при fetchSkills.fulfilled', () => {
      const previousState = {
        items: [],
        status: 'loading',
        error: null,
      };
      const fetchedSkills = initialSkills;
      expect(reducer(previousState, fetchSkills.fulfilled(fetchedSkills))).toEqual({
        ...previousState,
        status: 'succeeded',
        items: fetchedSkills,
      });
    });

    it('должен установить ошибку и статус при fetchSkills.rejected', () => {
      const previousState = {
        items: [],
        status: 'loading',
        error: null,
      };
      const error = { message: 'Ошибка при загрузке данных' };
      expect(reducer(previousState, fetchSkills.rejected(error))).toEqual({
        ...previousState,
        status: 'failed',
        error: error.message,
      });
    });
  });

  describe('thunks', () => {
    it('должен выполнить fetchSkills и сохранить данные в localStorage', async () => {
      const store = mockStore({
        items: [],
        status: 'idle',
        error: null,
      });

      const fetchedSkills = initialSkills;
      axios.get.mockResolvedValueOnce({ data: fetchedSkills });

      await store.dispatch(fetchSkills());

      const actions = store.getActions();
      expect(actions[0].type).toEqual(fetchSkills.pending.type);
      expect(actions[1].type).toEqual(fetchSkills.fulfilled.type);
      expect(actions[1].payload).toEqual(fetchedSkills);
      expect(localStorage.getItem('skills')).toEqual(JSON.stringify(fetchedSkills));
    });

    it('должен обработать ошибку при выполнении fetchSkills', async () => {
      const store = mockStore({
        items: [],
        status: 'idle',
        error: null,
      });

      const errorMessage = 'Ошибка при загрузке данных';
      axios.get.mockRejectedValueOnce(new Error(errorMessage));

      await store.dispatch(fetchSkills());

      const actions = store.getActions();
      expect(actions[0].type).toEqual(fetchSkills.pending.type);
      expect(actions[1].type).toEqual(fetchSkills.rejected.type);
      expect(actions[1].error.message).toEqual(errorMessage);
    });
  });
});
