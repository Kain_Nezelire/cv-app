import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSkills, addSkill } from './skillsSlice';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const SkillsWrapper = styled.div`
  padding: 20px;
`;

const SkillBar = styled.div`
  background: #4caf50;
  height: 30px;
  width: ${(props) => props.level}%;
  max-width: 100%;
  margin-bottom: 10px;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
`;

const SkillContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
`;

const EditButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  color: #333;
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

const SkillFormWrapper = styled.div`
  display: ${(props) => (props.visible ? 'block' : 'none')};
  margin-bottom: 20px;
  border: 1px solid #ccc;
  padding: 20px;
  border-radius: 5px;
  width: 100%;
`;

const Input = styled(Field)`
  display: block;
  margin-bottom: 10px;
  padding: 10px;
  width: 100%;
  border: 1px solid #ccc;
  border-radius: 5px;
`;

const AddSkillButton = styled.button`
  background-color: #4caf50;
  color: white;
  padding: 10px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
`;

const SkillMeter = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: 20px;
  font-size: 12px;
  color: #666;
`;

const Skills = () => {
  const dispatch = useDispatch();
  const skills = useSelector((state) => state.skills.items);
  const [isFormVisible, setFormVisible] = useState(false);

  useEffect(() => {
    dispatch(fetchSkills());
  }, [dispatch]);

  const validationSchema = Yup.object({
    skillName: Yup.string().required('Skill name is a required field'),
    skillRange: Yup.number()
      .required('Skill range is a required field')
      .min(10, 'Skill range must be between 10 and 100')
      .max(100, 'Skill range must be between 10 and 100'),
  });

  const handleAddSkill = (values, { resetForm }) => {
    dispatch(addSkill({ name: values.skillName, level: values.skillRange }));
    resetForm();
    setFormVisible(false);
  };

  return (
    <SkillsWrapper>
      <EditButton onClick={() => setFormVisible(!isFormVisible)}>
        <FontAwesomeIcon icon={faPen} />
        <span>Open edit</span>
      </EditButton>
      <Formik
        initialValues={{ skillName: '', skillRange: '' }}
        validationSchema={validationSchema}
        onSubmit={handleAddSkill}
      >
        {({ isValid }) => (
          <SkillFormWrapper visible={isFormVisible}>
            <Form>
              <div>
                <label htmlFor="skillName">Skill name</label>
                <Input type="text" name="skillName" placeholder="Enter skill name" />
                <ErrorMessage name="skillName" component="div" style={{ color: 'red' }} />
              </div>
              <div>
                <label htmlFor="skillRange">Skill range</label>
                <Input type="number" name="skillRange" placeholder="Enter skill range" />
                <ErrorMessage name="skillRange" component="div" style={{ color: 'red' }} />
              </div>
              <AddSkillButton type="submit" disabled={!isValid}>
                Add skill
              </AddSkillButton>
            </Form>
          </SkillFormWrapper>
        )}
      </Formik>
      <SkillContainer>
        {skills.map((skill) => (
          <div key={skill.id} style={{ width: '100%' }}>
            <SkillBar level={skill.level}>
              {skill.name}
            </SkillBar>
          </div>
        ))}
      </SkillContainer>
      <SkillMeter>
        <span>Beginner</span>
        <span>Proficient</span>
        <span>Expert</span>
        <span>Master</span>
      </SkillMeter>
    </SkillsWrapper>
  );
};

export default Skills;
