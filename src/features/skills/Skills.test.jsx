// src/features/skills/Skills.test.js

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import Skills from './skills';
import skillsReducer from './skillsSlice';

const store = configureStore({
  reducer: {
    skills: skillsReducer,
  },
});

test('renders Skills component', () => {
  render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );

  expect(screen.getByText(/Skills/i)).toBeInTheDocument();
});

test('can add a skill', () => {
  render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );

  fireEvent.change(screen.getByPlaceholderText(/Skill name/i), { target: { value: 'Test Skill' } });
  fireEvent.change(screen.getByPlaceholderText(/Skill range/i), { target: { value: '50' } });
  fireEvent.click(screen.getByText(/Add Skill/i));

  expect(screen.getByText(/Test Skill/i)).toBeInTheDocument();
  expect(screen.getByText(/50%/i)).toBeInTheDocument();
});
