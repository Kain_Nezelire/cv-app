// src/components/Contact/Contact.jsx
import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebook, faSkype } from '@fortawesome/free-brands-svg-icons';

const ContactWrapper = styled.div`
  padding: 20px;
`;

const ContactItem = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 15px;
  font-size: 1.2em;

  a {
    color: inherit;
    text-decoration: none;
    margin-left: 10px;

    &:hover {
      text-decoration: underline;
    }
  }

  svg {
    color: rgb(69,185,117);
  }
`;

const Contact = () => (
  <ContactWrapper>
    <ContactItem>
      <FontAwesomeIcon icon={faPhone} />
      <a href="tel:500342242">500 342 242</a>
    </ContactItem>
    <ContactItem>
      <FontAwesomeIcon icon={faEnvelope} />
      <a href="mailto:office@kamsolutions.pl">office@kamsolutions.pl</a>
    </ContactItem>
    <ContactItem>
      <FontAwesomeIcon icon={faTwitter} />
      <a href="https://twitter.com/wordpress" target="_blank" rel="noopener noreferrer">Twitter</a>
    </ContactItem>
    <ContactItem>
      <FontAwesomeIcon icon={faFacebook} />
      <a href="https://www.facebook.com/facebook" target="_blank" rel="noopener noreferrer">Facebook</a>
    </ContactItem>
    <ContactItem>
      <FontAwesomeIcon icon={faSkype} />
      <a href="skype:kamsolutions.pl?chat">Skype</a>
    </ContactItem>
  </ContactWrapper>
);

export default Contact;
