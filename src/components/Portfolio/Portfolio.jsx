// src/components/Portfolio/Portfolio.jsx
import React, { useEffect, useRef } from 'react';
import Isotope from 'isotope-layout';
import styled from 'styled-components';
import Card1 from '../../assets/images/card_1.png';
import Card2 from '../../assets/images/card_3.png';

const PortfolioWrapper = styled.div`
  @media (max-width: 768px) {
  }
`;

const FilterButtons = styled.div`
  margin-bottom: 20px;
  display: flex;
  justify-content: center;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

const FilterButton = styled.button`
  margin-right: 10px;
  padding: 10px 20px;
  cursor: pointer;
  background-color: #f0f0f0;
  border: none;
  border-radius: 4px;

  &:hover {
    background-color: #ccc;
  }

  @media (max-width: 768px) {
    margin-bottom: 10px;
    width: 100%;
  }
`;

const PortfolioItems = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const PortfolioItem = styled.div`
  margin: 1.5%;
  border-radius: 4px;
  position: relative;

  @media (max-width: 768px) {
    width: 45%;
    margin: 2.5%;
  }

  @media (max-width: 480px) {
    width: 90%;
    display: flex;
    flex-direction: column;
  }
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0;
  transition: opacity 0.3s;

  &:hover {
    opacity: 1;
  }
`;

const Portfolio = () => {
  const isotope = useRef(null);
  const portfolioItems = useRef(null);

  useEffect(() => {
    if (portfolioItems.current) {
      isotope.current = new Isotope(portfolioItems.current, {
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows',
      });
    }

    return () => {
      if (isotope.current) {
        isotope.current.destroy();
      }
    };
  }, []);

  useEffect(() => {
    if (isotope.current) {
      isotope.current.layout();
    }
  });

  const handleFilter = (filter) => {
    isotope.current.arrange({ filter: filter === '*' ? '*' : `.${filter}` });
  };

  return (
    <PortfolioWrapper>
      <FilterButtons>
        <FilterButton onClick={() => handleFilter('*')}>All</FilterButton>
        <FilterButton onClick={() => handleFilter('ui')}>UI</FilterButton>
        <FilterButton onClick={() => handleFilter('code')}>Code</FilterButton>
      </FilterButtons>
      <PortfolioItems ref={portfolioItems}>
        <PortfolioItem className="portfolio-item ui">
          <img src={Card1} alt="UI Project 1" />
          <Overlay>UI Project 1</Overlay>
        </PortfolioItem>
        <PortfolioItem className="portfolio-item code">
          <img src={Card2} alt="Code Project 1" />
          <Overlay>Code Project 1</Overlay>
        </PortfolioItem>
        <PortfolioItem className="portfolio-item ui">
          <img src={Card1} alt="UI Project 2" />
          <Overlay>UI Project 2</Overlay>
        </PortfolioItem>
        <PortfolioItem className="portfolio-item code">
          <img src={Card2} alt="Code Project 2" />
          <Overlay>Code Project 2</Overlay>
        </PortfolioItem>
        <PortfolioItem className="portfolio-item ui">
          <img src={Card1} alt="UI Project 3" />
          <Overlay>UI Project 3</Overlay>
        </PortfolioItem>
      </PortfolioItems>
    </PortfolioWrapper>
  );
};

export default Portfolio;
