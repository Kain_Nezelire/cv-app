import styled from "styled-components";

export const PhotoContainer = styled.div`
  img {
    width: ${props => props.size};
    border-radius: ${props => props.borderRadius};
    border: ${props => props.border};
  }
`;
