import React from 'react';
import photo from '../../assets/images/Avatar.png';
import { PhotoContainer } from './PhotoBoxStyle';

const PhotoBox = ({ size = '120px', borderRadius = '300px', border = '2px solid white' }) => {
  return (
    <PhotoContainer size={size} borderRadius={borderRadius} border={border}>
      <img src={photo} alt='logo' />
    </PhotoContainer>
  );
};

export default PhotoBox;
