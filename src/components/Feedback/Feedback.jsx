import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import photo from '../../assets/Ellipse 1.png';

const FeedbackWrapper = styled.div`
  padding: 20px;
`;

const FeedbackItemWrapper = styled.div`
  margin-bottom: 5px;
`;

const FeedbackItem = styled.div`
  padding: 15px;
  background-color: #f9f9f9;
  border-radius: 4px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const FeedbackContent = styled.div`
  display: flex;
  flex-direction: column;
`;

const FeedbackText = styled.p`
  margin: 0;
  margin-bottom: 10px;
  color: #555;
`;

const Reporter = styled.div`
  display: flex;
  align-items: center;
  margin-top: 10px;
`;

const Photo = styled.img`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  margin-right: 15px;
`;

const ReporterDetails = styled.div`
  display: flex;
  align-items: center;
`;

const ReporterName = styled.span`
  font-weight: bold;
  margin-right: 5px;
`;

const CiteUrl = styled.a`
  color: #1abc9c;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`;

const Feedback = ({ data }) => (
  <FeedbackWrapper>
    {data.map((item, index) => (
      <FeedbackItemWrapper key={index}>
        <FeedbackItem>
          <FeedbackContent>
            <FeedbackText>{item.feedback}</FeedbackText>
          </FeedbackContent>
        </FeedbackItem>
        <Reporter>
          <Photo src={photo} alt={item.reporter.name} />
          <ReporterDetails>
            <ReporterName>{item.reporter.name}</ReporterName>
            <CiteUrl href={item.reporter.citeUrl} target="_blank" rel="noopener noreferrer">
              {item.reporter.citeUrl}
            </CiteUrl>
          </ReporterDetails>
        </Reporter>
      </FeedbackItemWrapper>
    ))}
  </FeedbackWrapper>
);

Feedback.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      feedback: PropTypes.string.isRequired,
      reporter: PropTypes.shape({
        photoUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        citeUrl: PropTypes.string.isRequired,
      }).isRequired,
    })
  ).isRequired,
};

export default Feedback;
