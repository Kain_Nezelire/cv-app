    import styled from "styled-components";


export const StyledButton = styled.button`
  padding: 10px 20px;
  width:127px;
  height:40px;
  background-color: #222935;
  color: white;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  font-size: 16px;
  &:hover {
    background-color:rgb(71,184,118);
  }
  &:focus {
    outline: none;
  }
`;
