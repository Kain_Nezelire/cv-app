import styled from "styled-components";

export const BoxWrapper = styled.div`
  padding: 20px;

  background-color: #fff;
  margin-bottom: 20px;
`;

export const BoxTitle = styled.h2`
  margin-bottom: 10px;
  color:rgb(71,184,118);
  font-weight: bold;
  
`;

export const BoxContent = styled.p`
  font-size: 14px;
  color: #555;
`;