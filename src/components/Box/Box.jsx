// src/components/Box/index.js
import React from 'react';
import PropTypes from 'prop-types';
import { BoxContent , BoxTitle, BoxWrapper} from './BoxStyle';

const Box = ({ title, content }) => (
    <BoxWrapper>
      <BoxTitle>{title}</BoxTitle>
      <BoxContent>{content}</BoxContent>
    </BoxWrapper>
  );
  
  Box.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
  };
  
  export default Box;
