// src/components/Education/Education.jsx

import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { useSelector } from 'react-redux';
import TimeLine from '../Timeline/Timeline';

const Section = styled.section`
  padding: 20px 0;
`;

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100px;
  background: rgba(255, 255, 255, 0.8);
`;

const Education = () => {
  const [loading, setLoading] = useState(true);
  const timelineData = useSelector((state) => state.timeline.items);

  useEffect(() => {
    const timer = setTimeout(() => setLoading(false), 3000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Section>
      {loading ? (
        <LoaderWrapper>
          <FontAwesomeIcon className="icon" icon={faSyncAlt} spin size="2x" />
        </LoaderWrapper>
      ) : (
        <TimeLine data={timelineData} />
      )}
    </Section>
  );
};

export default Education;
