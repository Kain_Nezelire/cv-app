import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGraduationCap, faBriefcase, faLightbulb, faFolderOpen, faPaperPlane, faComments, faUser, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-scroll';
import PhotoBox from '../PhotoBox/PhotoBox';
import Button from '../Button/Button';
import { useNavigate } from 'react-router-dom';

const Sidebar = styled.div`
  height: 100vh;
  width: ${props => (props.isOpen ? '250px' : '60px')};
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgb(33, 36, 46);
  overflow-x: hidden;
  transition: width 0.3s;
  padding-top: 20px;
  color: white;
  z-index: 1000; 
  display: flex;
  flex-direction: column;
  align-items: ${props => (props.isOpen ? 'flex-start' : 'center')};
`;

const SidebarHeader = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-bottom: 20px;
`;

const SidebarItem = styled(Link)`
  width: 100%;
  padding: 10px 20px;
  color: rgb(94, 99, 117);
  display: flex;
  align-items: center;
  justify-content: ${props => (props.isOpen ? 'flex-start' : 'center')};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  cursor: pointer;
  transition: all 0.3s;
  font-size: 1em;

  &:hover {
    background-color: #34495e;
    color: rgb(69, 185, 117);
  }

  svg {
    margin-right: ${props => (props.isOpen ? '10px' : '0')};
    transition: margin-right 0.3s;
  }
`;

const BottomButton = styled.div`
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
  width: ${props => (props.isOpen ? '90%' : '30px')};
  display: flex;
  justify-content: center;
  transition: all 0.3s;

  button {
    width: 100%;
  }
`;

const Navigation = ({ isOpen }) => {
  const navigate = useNavigate();

  const handleButtonClick = () => {
    navigate('/');
  };

  return (
    <Sidebar isOpen={isOpen}>
      <SidebarHeader>
        <PhotoBox
          size={isOpen ? '80px' : '40px'}
          borderRadius="50%"
          border="2px solid white"
        />
      </SidebarHeader>
      <SidebarItem to="about" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faUser} />
        {isOpen && "About me"}
      </SidebarItem>
      <SidebarItem to="education" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faGraduationCap} />
        {isOpen && "Education"}
      </SidebarItem>
      <SidebarItem to="experience" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faBriefcase} />
        {isOpen && "Experience"}
      </SidebarItem>
      <SidebarItem to="skills" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faLightbulb} />
        {isOpen && "Skills"}
      </SidebarItem>
      <SidebarItem to="portfolio" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faFolderOpen} />
        {isOpen && "Portfolio"}
      </SidebarItem>
      <SidebarItem to="contacts" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faPaperPlane} />
        {isOpen && "Contacts"}
      </SidebarItem>
      <SidebarItem to="feedback" smooth={true} duration={500} isOpen={isOpen}>
        <FontAwesomeIcon icon={faComments} />
        {isOpen && "Feedbacks"}
      </SidebarItem>
      <BottomButton isOpen={isOpen}>
        <Button icon={<FontAwesomeIcon icon={faChevronLeft} />} buttonText={isOpen ? "Go back" : ""} onClick={handleButtonClick} />
      </BottomButton>
    </Sidebar>
  );
};

export default Navigation;
