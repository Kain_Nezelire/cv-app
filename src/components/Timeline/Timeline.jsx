import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchTimeline } from '../../features/timeline/TimelineSlice';
import {
  TimeLineWrapper,
  TimeLineItem,
  TimeLineDate,
  TimeLineTitle,
  TimeLineText,
  LoaderWrapper,
  LoaderIcon,
  ErrorMessage,
} from './TimelineStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

const Timeline = () => {
  const dispatch = useDispatch();
  const { items, status, error } = useSelector((state) => state.timeline);

  useEffect(() => {
    dispatch(fetchTimeline());
  }, [dispatch]);

  if (status === 'loading') {
    return (
      <LoaderWrapper>
        <LoaderIcon>
          <FontAwesomeIcon icon={faSpinner} spin />
        </LoaderIcon>
      </LoaderWrapper>
    );
  }

  if (status === 'failed') {
    return <ErrorMessage>{error}</ErrorMessage>;
  }

  return (
    <TimeLineWrapper>
      {items.map((item) => (
        <TimeLineItem key={item.id}>
          <TimeLineDate>{item.date}</TimeLineDate>
          <TimeLineTitle>{item.title}</TimeLineTitle>
          <TimeLineText>{item.text}</TimeLineText>
        </TimeLineItem>
      ))}
    </TimeLineWrapper>
  );
};

export default Timeline;
