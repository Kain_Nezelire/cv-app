// src/components/TimeLine/styles.js
import styled from 'styled-components';

export const TimeLineWrapper = styled.div`
  max-height: 50vh;
  overflow-y: auto;
`;

export const TimeLineItem = styled.div`
  position: relative;
  padding-left: 60px; 
  margin: 30px 0;
  &:before {
    content: '';
    position: absolute;
    left: 30px; 
    top: 0;
    bottom: 0;
    width: 4px;
    background-color: #4CAF50;
  }

  @media (max-width: 480px) {
    padding-left: 0;
  }
`;

export const TimeLineDate = styled.div`
  position: absolute;
  left: 7px; 
  top: 0;
  font-weight: bold;
  color: #4CAF50;
  background: white;
  padding: 5px 10px;
  @media (max-width: 480px) {
    top: -20px;
  }
`;

export const TimeLineBox = styled.div`
  background-color: #f0f0f0;
  padding: 15px;
  border-radius: 4px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  margin-left: 60px; 
`;

export const TimeLineTitle = styled.div`
  font-weight: bold;
  margin-bottom: 20px;
  color: #333;
`;

export const TimeLineText = styled.div`
  color: #555;
`;

export const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: rgba(255, 255, 255, 0.8);
`;

export const LoaderIcon = styled.div`
  font-size: 24px;
  color: #36cfc9;
`;

export const ErrorMessage = styled.div`
  color: red;
  text-align: center;
`;
