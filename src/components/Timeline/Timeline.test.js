// src/components/Timeline/Timeline.test.js

import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import Timeline from './Timeline';
import timelineReducer from '../../features/timeline/TimelineSlice';

const store = configureStore({
  reducer: {
    timeline: timelineReducer,
  },
});

test('renders Timeline component', () => {
  render(
    <Provider store={store}>
      <Timeline />
    </Provider>
  );

  expect(screen.getByText(/Timeline/i)).toBeInTheDocument();
});
