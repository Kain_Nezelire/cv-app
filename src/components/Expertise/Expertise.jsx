// src/components/Expertise/index.js
import React from 'react';
import PropTypes from 'prop-types';
import {
  ExpertiseWrapper,
  ExperienceItem,
  Company,
  JobTitle,
  Date,
  Description,
} from './ExpertiseStyle';

const Expertise = ({ data }) => (
  <ExpertiseWrapper>
    {data.map((item, index) => (
      <ExperienceItem key={index}>
        <div>
        <Company>{item.info.company}</Company>
        <Date>{item.date}</Date>
        </div>
        <div>
        <JobTitle>{item.info.job}</JobTitle>
        <Description>{item.description}</Description>
        </div>
      </ExperienceItem>
    ))}
  </ExpertiseWrapper>
);

Expertise.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.string.isRequired,
      info: PropTypes.shape({
        company: PropTypes.string.isRequired,
        job: PropTypes.string.isRequired,
      }).isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default Expertise;
