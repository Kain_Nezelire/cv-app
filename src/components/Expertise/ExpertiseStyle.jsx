// src/components/Expertise/styles.js
import styled from 'styled-components';

export const ExpertiseWrapper = styled.div`
  margin: 20px 0;
`;

export const ExperienceItem = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 20px;

  >div:first-child{
    margin:10px;
  }

  >div:last-child{
    margin:10px;
  }
`;

export const Company = styled.div`
  font-weight: bold;
  color: #333;
  font-size: 1.2em;
`;

export const JobTitle = styled.div`
  font-weight: bold;
  color: #333;
  font-size: 1em;
  margin-bottom: 5px;
`;

export const Date = styled.div`
  color: #888;
  font-size: 0.9em;
  margin-bottom: 5px;
`;

export const Description = styled.div`
  color: #555;
  font-size: 0.9em;
`;
